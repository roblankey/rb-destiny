# rb-destiny
A Ruby application providing the capability to construct decks for Star Wars: Destiny

## Dependencies
* [boot-destiny](https://bitbucket.org/roblankey/boot-destiny) - used for the back end card information
* [sqlite](https://www.sqlite.org/index.html) - used for storing decks

